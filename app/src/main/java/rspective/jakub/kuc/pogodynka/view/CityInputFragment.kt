package rspective.jakub.kuc.pogodynka.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_city_pick.*
import rspective.jakub.kuc.pogodynka.R
import rspective.jakub.kuc.pogodynka.dagger.AppComponent
import rspective.jakub.kuc.pogodynka.data.SelectedLocationRepository
import rx.Subscriber
import rx.Subscription
import timber.log.Timber
import javax.inject.Inject

class CityInputFragment : Fragment() {

    @Inject
    protected lateinit var selectedLocationRepo: SelectedLocationRepository

    private lateinit var locationSubscribtion: Subscription;

    companion object {
        fun newInstance(): Fragment = CityInputFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.instance.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_city_pick, container, false) as LinearLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationSubscribtion = selectedLocationRepo.subscribeSelectedLocation(CurrentLocationSubscriber())

        city_confirm.setOnClickListener {
            hideKeyboard()
            selectedLocationRepo.changeLocation(city_input.text.toString())
            setCurrentLocationText(city_input.text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        locationSubscribtion.unsubscribe()
    }

    private fun hideKeyboard() {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
        imm.hideSoftInputFromWindow(city_input.windowToken, 0);
    }

    private fun setCurrentLocationText(location: String) {
        current_location_id.text = "${activity.resources.getString(R.string.current_selection)} $location"
    }

    private inner class CurrentLocationSubscriber : Subscriber<String>() {

        override fun onNext(t: String) {
            setCurrentLocationText(t)
        }

        override fun onError(e: Throwable) {
            Timber.d(e.message)
        }

        override fun onCompleted() {

        }
    }
}