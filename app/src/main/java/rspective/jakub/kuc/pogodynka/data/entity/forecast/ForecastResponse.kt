package rspective.jakub.kuc.pogodynka.data.entity.forecast

data class ForecastResponse(val list: List<DailyForecastResponse>)