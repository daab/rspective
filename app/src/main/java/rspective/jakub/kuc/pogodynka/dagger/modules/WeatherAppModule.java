package rspective.jakub.kuc.pogodynka.dagger.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rspective.jakub.kuc.pogodynka.data.SelectedLocationRepository;
import rspective.jakub.kuc.pogodynka.data.SelectedLocationRepositoryImpl;

@Module
public class WeatherAppModule {

	private final Context context;

	public WeatherAppModule(Context context) {
		this.context = context;
	}

	@Provides
	public Context providesAppContext() {
		return context;
	}

	@Provides
	@Singleton
	public SelectedLocationRepository providesSelectedLocationRepo(Context context) {
		return new SelectedLocationRepositoryImpl(context);
	}
}
