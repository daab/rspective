package rspective.jakub.kuc.pogodynka.data.entity.current

data class MainInfo(val temp: Float, val temp_max: Float, val temp_min: Float, val pressure: Float, val humidity: Float)