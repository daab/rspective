package rspective.jakub.kuc.pogodynka.data.services

import retrofit.http.GET
import retrofit.http.Query
import rspective.jakub.kuc.pogodynka.BuildConfig
import rspective.jakub.kuc.pogodynka.data.entity.current.TodayForecastResponse
import rspective.jakub.kuc.pogodynka.data.entity.forecast.ForecastResponse
import rx.Observable

interface WeatherService {

    @GET("daily")
    fun getWeekForecast(@Query("q") city: String,
                        @Query("units") units: String = "metric",
                        @Query("cnt") count: Int = 5,
                        @Query("APPID") appId: String = BuildConfig.WEATHER_API_KEY): Observable<ForecastResponse>

    @GET("weather")
    fun getCurrentForecast(@Query("q") city: String,
                           @Query("units") units: String = "metric",
                           @Query("cnt") count: Int = 5,
                           @Query("APPID") appId: String = BuildConfig.WEATHER_API_KEY): Observable<TodayForecastResponse>
}