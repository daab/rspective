package rspective.jakub.kuc.pogodynka.data

import rx.Subscriber
import rx.Subscription

interface SelectedLocationRepository{

    fun subscribeSelectedLocation(subscriber: Subscriber<String>): Subscription

    fun changeLocation(location: String)

}