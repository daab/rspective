package rspective.jakub.kuc.pogodynka.data.entity.forecast

data class DailyForecastResponse(val temp: TemperatureInfoReponse, val pressure: Float, val humidity: Float)