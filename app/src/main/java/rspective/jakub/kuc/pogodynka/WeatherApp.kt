package rspective.jakub.kuc.pogodynka

import android.app.Application
import rspective.jakub.kuc.pogodynka.dagger.AppComponent
import timber.log.Timber

class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()

        AppComponent.init(this)
        Timber.plant(Timber.DebugTree())
    }

}