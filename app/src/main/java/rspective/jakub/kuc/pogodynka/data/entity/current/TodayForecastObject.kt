package rspective.jakub.kuc.pogodynka.data.entity.current

import rspective.jakub.kuc.pogodynka.data.entity.WeatherDescription

data class TodayForecastObject(val main: MainInfo, val weather: List<WeatherDescription>)