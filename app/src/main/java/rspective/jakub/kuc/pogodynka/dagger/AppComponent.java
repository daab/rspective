package rspective.jakub.kuc.pogodynka.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import rspective.jakub.kuc.pogodynka.dagger.modules.RetrofitModule;
import rspective.jakub.kuc.pogodynka.dagger.modules.WeatherAppModule;
import rspective.jakub.kuc.pogodynka.view.CityInputFragment;
import rspective.jakub.kuc.pogodynka.view.CurrentWeatherFragment;
import rspective.jakub.kuc.pogodynka.view.ForecastFragment;

@Singleton
@Component(modules = {WeatherAppModule.class, RetrofitModule.class})
public abstract class AppComponent {

	public static AppComponent instance;

	public static void init(Context context) {
		instance = DaggerAppComponent.builder()
				.weatherAppModule(new WeatherAppModule(context))
				.retrofitModule(new RetrofitModule())
				.build();
	}

	public abstract void inject(ForecastFragment forecastFragment);

	public abstract void inject(CurrentWeatherFragment CurrentWeatherFragment);

	public abstract void inject(CityInputFragment cityInputFragment);
}
