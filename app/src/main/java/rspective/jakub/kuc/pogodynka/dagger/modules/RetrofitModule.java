package rspective.jakub.kuc.pogodynka.dagger.modules;

import com.squareup.moshi.Moshi;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.List;

import dagger.Module;
import dagger.Provides;
import retrofit.MoshiConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rspective.jakub.kuc.pogodynka.BuildConfig;
import rspective.jakub.kuc.pogodynka.data.services.WeatherService;
import timber.log.Timber;

@Module
public class RetrofitModule {

	@Provides
	public Retrofit providesRetrofit(Moshi moshi) {

		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
			@Override
			public void log(String message) {
				Timber.tag("RetroLogger").d(message);
			}
		});

		loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.valueOf(BuildConfig.RETROFIT_LOG_LEVEL));

		OkHttpClient okHttpClient = new OkHttpClient();
		List<Interceptor> interceptors = okHttpClient.networkInterceptors();
		interceptors.add(loggingInterceptor);

		return new Retrofit.Builder()
				.client(okHttpClient)
				.baseUrl(BuildConfig.BACKEND_URL)
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(MoshiConverterFactory.create(moshi))
				.build();
	}

	@Provides
	public WeatherService providesWheaderService(Retrofit retrofit) {
		return retrofit.create(WeatherService.class);
	}


	@Provides
	public Moshi providesMoshiWithAdapters() {
		return new Moshi.Builder()
				.build();
	}
}
