package rspective.jakub.kuc.pogodynka.presenter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import rspective.jakub.kuc.pogodynka.view.CityInputFragment
import rspective.jakub.kuc.pogodynka.view.CurrentWeatherFragment
import rspective.jakub.kuc.pogodynka.view.ForecastFragment

class WeatherPagerAdapter(val fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int = 3

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "City"
            1 -> return "Today"
            else -> return "Next 4 days"
        }
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return CityInputFragment.newInstance()
            1 -> return CurrentWeatherFragment.newInstance()
            else -> return ForecastFragment.newInstance()
        }
    }
}