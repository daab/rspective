package rspective.jakub.kuc.pogodynka.data.entity.current

data class TodayForecastResponse(val list: List<TodayForecastObject>)