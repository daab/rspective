package rspective.jakub.kuc.pogodynka

enum class WeatherTabEnum(val layour_res_id: Int) {

    CITY(R.layout.view_city_pick),
    CURRENT_WEATHER(R.layout.view_current_weather)
}