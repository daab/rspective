package rspective.jakub.kuc.pogodynka.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.view_current_weather.*
import rspective.jakub.kuc.pogodynka.R
import rspective.jakub.kuc.pogodynka.dagger.AppComponent
import rspective.jakub.kuc.pogodynka.data.SelectedLocationRepository
import rspective.jakub.kuc.pogodynka.data.entity.current.MainInfo
import rspective.jakub.kuc.pogodynka.data.services.WeatherService
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import javax.inject.Inject

class CurrentWeatherFragment : Fragment() {

    @Inject
    protected lateinit var selectedLocationRepo: SelectedLocationRepository

    @Inject
    protected lateinit var weatherService: WeatherService

    private val subs: CompositeSubscription = CompositeSubscription()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.instance.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_current_weather, container, false) as RelativeLayout
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sub = selectedLocationRepo.subscribeSelectedLocation(CurrentLocationSubscriber())
        subs.add(sub)
    }

    private fun obtainWeaterForecast(location: String) {
        val sub = weatherService.getCurrentForecast(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            fillData(it.list[0].main)
                            weather.text = it.list[0].weather[0].description
                        },
                        {
                            Timber.d(it.message)
                        }

                )

        subs.add(sub)
    }

    private fun fillData(forecastResponse: MainInfo) {
        humidity.text = "hum.: ${Math.round(forecastResponse.humidity)} %"
        pressure.text = "atm.: ${forecastResponse.pressure} hpa"
        min_temp.text = "▼ ${Math.round(forecastResponse.temp_min)}°"
        max_temp.text = "▲ ${Math.round(forecastResponse.temp_max)}°"
        current_temp.text = "${Math.round(forecastResponse.temp)}°"
    }

    override fun onDestroy() {
        super.onDestroy()
        subs.unsubscribe()
    }

    private inner class CurrentLocationSubscriber : Subscriber<String>() {

        override fun onNext(t: String) {
            obtainWeaterForecast(t)
        }

        override fun onError(e: Throwable) {
            Timber.d(e.message)
        }

        override fun onCompleted() {

        }
    }

    companion object {
        fun newInstance(): Fragment = CurrentWeatherFragment()
    }
}