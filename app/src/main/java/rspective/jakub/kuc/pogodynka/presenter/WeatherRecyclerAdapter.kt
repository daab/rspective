package rspective.jakub.kuc.pogodynka.presenter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import rspective.jakub.kuc.pogodynka.R
import rspective.jakub.kuc.pogodynka.data.entity.forecast.DailyForecastResponse

class WeatherRecyclerAdapter(val forecast: List<DailyForecastResponse>, context: Context) : RecyclerView.Adapter<WeatherRecyclerAdapter.WeatherHolder>() {

    val inflater: LayoutInflater
    val timeFormat: DateTimeFormatter = DateTimeFormat.forPattern("dd:MM")

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun onBindViewHolder(holder: WeatherHolder, position: Int) {
        holder.temperature.text = "${Math.round(forecast[position].temp.day)}°"
        holder.min.text = "▼ ${Math.round(forecast[position].temp.min)}°"
        holder.max.text = "▲ ${Math.round(forecast[position].temp.max)}°"
        holder.date.text = timeFormat.print(DateTime.now().plusDays(1 + position))
    }

    override fun getItemCount(): Int = forecast.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherHolder {
        val itemView = inflater.inflate(R.layout.forecast_item, parent, false)
        return WeatherHolder(itemView)
    }

    class WeatherHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val temperature: TextView
        val min: TextView
        val max: TextView
        val date: TextView

        init {
            temperature = itemView.findViewById(R.id.temp) as TextView
            min = itemView.findViewById(R.id.temp_min) as TextView
            max = itemView.findViewById(R.id.temp_max) as TextView
            date = itemView.findViewById(R.id.date) as TextView
        }
    }
}