package rspective.jakub.kuc.pogodynka.data

import android.content.Context
import android.content.SharedPreferences
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject

class SelectedLocationRepositoryImpl(context: Context) : SelectedLocationRepository {

    private var prefs: SharedPreferences
    private val locationSubject: BehaviorSubject<String>

    init {
        prefs = context.getSharedPreferences("WeatherAppSharedPrefs", Context.MODE_PRIVATE)
        locationSubject = BehaviorSubject.create(prefs.getString(locationKey, "Katowice"))
    }

    override fun changeLocation(location: String) {
        prefs.edit().putString(locationKey, location).commit()
        locationSubject.onNext(location)
    }

    override fun subscribeSelectedLocation(subscriber: Subscriber<String>) =
            locationSubject
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(subscriber)


    companion object {
        val locationKey: String = "LOCATION_KEY"
    }
}