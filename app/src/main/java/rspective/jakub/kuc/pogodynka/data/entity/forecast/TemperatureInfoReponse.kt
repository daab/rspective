package rspective.jakub.kuc.pogodynka.data.entity.forecast

data class TemperatureInfoReponse(val day: Float, val min: Float, val max: Float)