package rspective.jakub.kuc.pogodynka.view

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import rspective.jakub.kuc.pogodynka.R
import rspective.jakub.kuc.pogodynka.presenter.WeatherPagerAdapter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        weather_pager.adapter = WeatherPagerAdapter(supportFragmentManager)
        weather_tabs.setupWithViewPager(weather_pager)
        (weather_tabs.getTabAt(1) as TabLayout.Tab).select()
    }
}
